package models;

import java.util.ArrayList;
import java.util.List;

public class Node {

        private String name;
        private List<Edge> edges;

        public Node(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Edge> getEdges() {
            return edges;
        }

        public void addEdge(Edge edge) {
            if (edges == null) {
                edges = new ArrayList<>();
            }
            edges.add(edge);
        }

        @Override
        public String toString() {
            return "\n \tNode [name=" + name + ", Edges " + edges +"]";
        }
    }

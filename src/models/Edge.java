package models;

public class Edge {

    private Node origin;
    private Node destination;
    private double weight;

    public Edge(Node origin, Node destination, double weight) {
        this.origin = origin;
        this.destination = destination;
        this.weight = weight;
    }

    public Node getOrigin() {
        return origin;
    }

    public void setOrigin(Node origin) {
        this.origin = origin;
    }

    public Node getDestination() {
        return destination;
    }

    public void setDestination(Node destination) {
        this.destination = destination;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "\n \t \t Edge [origin=" + origin.getName() + ", destination=" + destination.getName() + ", weight="
                + weight + "]";
    }
}

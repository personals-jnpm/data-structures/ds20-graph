import models.Edge;
import models.Graph;
import models.Node;

import java.util.ArrayList;
import java.util.List;

public class Main {

    private final List<Node> nodesAlreadyPrinted = new ArrayList<>();
    private final List<Node> nodesAlreadyTraveled = new ArrayList<>();

    public static Graph initGraph() {
        Node steven = new Node("Steven");
        Node carlos = new Node("Carlos");
        Node andres = new Node("Andrés");
        Node catalina = new Node("Catalina");
        Node alejandra = new Node("Alejandra");
        Node jose = new Node("José");
        Node felipe = new Node("Felipe");
        Node jaider = new Node("Jaider");
        Node monica = new Node("Mónica");
        Node vanessa = new Node("Vanessa");

        steven.addEdge(new Edge(steven, carlos, 5));
        steven.addEdge(new Edge(steven, andres, 3));
        carlos.addEdge(new Edge(carlos, andres, 7));
        carlos.addEdge(new Edge(carlos, catalina, 3));
        carlos.addEdge(new Edge(carlos, alejandra, 8));
        andres.addEdge(new Edge(andres, catalina, 2));
        catalina.addEdge(new Edge(catalina, alejandra, 16));
        monica.addEdge(new Edge(monica, catalina, 9));
        jose.addEdge(new Edge(jose, alejandra, 8));
        felipe.addEdge(new Edge(felipe, andres, 13));
        jaider.addEdge(new Edge(jaider, steven, 8));
        jaider.addEdge(new Edge(jaider, andres, 5));
        jaider.addEdge(new Edge(jaider, carlos, 1.7));
        vanessa.addEdge(new Edge(vanessa, carlos, 6));
        alejandra.addEdge(new Edge(alejandra, jaider, 2));

        Graph graph = new Graph();
        graph.addNode(steven);
        graph.addNode(carlos);
        graph.addNode(andres);
        graph.addNode(catalina);
        graph.addNode(alejandra);
        graph.addNode(jose);
        graph.addNode(felipe);
        graph.addNode(jaider);
        graph.addNode(monica);
        graph.addNode(vanessa);
        return graph;
    }

    public static void main(String[] args) {
        Main main = new Main();

        Graph graph = initGraph();

        for (Node node : graph.getNodes()) {
            main.printGraphDepthForm(node);
        }

        Node node = main.findNodeByName("Carlos", graph);
        if (node != null) {
            System.out.println("\nNodo encontrado: " + node);
            Edge majorEdge = main.findEdgeByMaxWeight(node);
            System.out.println("\nEdge mayor: " + majorEdge);
        } else {
            System.out.println("\n¡Nodo no encontrado!");
        }
    }

    public void printGraphDepthForm(Node node) {
        if (!nodesAlreadyPrinted.contains(node) && node.getEdges() != null) {
            for (Edge edge : node.getEdges()) {
                System.out.println(edge.getOrigin());
                nodesAlreadyPrinted.add(node);
                printGraphDepthForm(edge.getDestination());
                return;
            }
        }
    }

    public Node findNodeByName(String name, Graph graph) {
        for (Node node : graph.getNodes()) {
            Node nodeFound = searchNode(node, name);
            if (nodeFound != null) {
                return nodeFound;
            }
        }
        return null;
    }

    public Node searchNode(Node node, String name) {
        if (!nodesAlreadyTraveled.contains(node) && node.getEdges() != null) {
            for (Edge edge : node.getEdges()) {
                if (edge.getOrigin().getName().equals(name)) {
                    return node;
                }
                nodesAlreadyTraveled.add(node);
                return searchNode(edge.getDestination(), name);
            }
        }
        return null;
    }

    public Edge findEdgeByMaxWeight(Node node) {
        if (node.getEdges() != null){
            Edge majorEdge = node.getEdges().get(0);
            double majorWeight = node.getEdges().get(0).getWeight();

            for (Edge edge : node.getEdges()) {
                if (majorWeight < edge.getWeight()) {
                    majorEdge = edge;
                }
            }
            return majorEdge;
        }
        return null;
    }
}
